import React from 'react';
import UserList from '../containers/user-list';
import UserDetail from '../containers/user-detail';
import UserAdd from '../containers/user-add';
require('../../scss/style.scss');

const App = () => {
	return (
		<div className="container">
			<h2>Username list App</h2>
			<hr />
			<UserAdd />
			<section className="row">
				<div className="col-xs-6">
					<h2>Username list:</h2>
					<hr />
					<UserList />
				</div>
				<div className="col-xs-6">
					<h2>User list:</h2>
					<UserDetail />
				</div>
			</section>
		</div>
	)
};

export default App;