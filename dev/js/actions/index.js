export const selectUser = (user) => {
	console.log("You've just clicked on " + user.first);
	return {
		type: "USER_SELECTED",
		payload: user
	}
};

export const addUser = (user) => {
	return {
		type: "ADD_USER",
		payload: user
	}
};