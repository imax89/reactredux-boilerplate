let users_array = [
	{
		id: 1,
		first: "Bucky",
		last: "Roberts",
		age: 71,
		description: "Bucky is a React developer and YouTuber",
		thumbnail: "http://i.imgur.com/7yUvePI.jpg"
	},
	{
		id: 2,
		first: "Joby",
		last: "Wasilenko",
		age: 27,
		description: "Joby loves the Packers, cheese, and turtles.",
		thumbnail: "http://i.imgur.com/52xRlm8.png"
	},
	{
		id: 3,
		first: "Madison",
		last: "Williams",
		age: 24,
		description: "Madi likes her dog but it is really annoying.",
		thumbnail: "http://i.imgur.com/4EMtxHB.png"
	}
];

export default function (state = users_array, action) {



	switch (action.type) {
		case "ADD_USER":
			let new_user = {
				id : state.length + 1,
				first: action.payload.first,
				last: action.payload.last,
				age: action.payload.age,
				description: action.payload.description,
				thumbnail: action.payload.thumbnail,
			};
			console.log('==>',new_user);
			if (!state.find((user)=>{
				return user.id == new_user.id
				})
			) {
				return [
					...state,
					new_user
				];
			} else {
				return state;
			}

			// state.update();
			// console.log('DATA:', users_array);

			break;
	}

	return state;
}