import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {addUser} from "../actions/index";

class UserAdd extends Component {

	constructor(){
		super();
		this.state = {
			new_user_data: {
				nu_first_name: '',
				nu_last_name: '',
				nu_age: '',
				nu_description: '',
				nu_thumbnail: '',
			}
		};
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
	}

	handleSubmit(e){
		e.preventDefault();
		let hasEmptyProps = false;
		Object.values(this.state.new_user_data).map((property)=>{
			if (!property.trim()) hasEmptyProps = true;
		});
		if (!hasEmptyProps) {
			let payload = {
				first: this.state.new_user_data.nu_first_name,
				last: this.state.new_user_data.nu_last_name,
				age: this.state.new_user_data.nu_age,
				description: this.state.new_user_data.nu_description,
				thumbnail: this.state.new_user_data.nu_thumbnail
			};
			this.props.addUser(payload);
			this.state.new_user_data = {
				nu_first_name: '',
				nu_last_name: '',
				nu_age: '',
				nu_description: '',
				nu_thumbnail: '',
			};
			this.setState(this.state);
		}
	}

	handleInputChange(e){
		this.state.new_user_data[e.target.name] = e.target.value;
		this.setState(this.state, ()=>{
				console.log(this.state);
		});
	}

	render(){
		return(
			<section>
				<form autoComplete="off" className="row" onSubmit={this.handleSubmit}>
					<div className="col-sm-3">
						<div className="input-group form-control well well-sm">
							<label className="input-group-addon">First name: </label>
							<input  name="nu_first_name" className="form-control" onChange={this.handleInputChange.bind(this)} value={this.state.new_user_data.nu_first_name} />
						</div>
					</div>
					<div className="col-sm-3">
						<div className="input-group form-control well well-sm">
							<label className="input-group-addon">Last name: </label>
							<input name="nu_last_name" className="form-control" onChange={this.handleInputChange} value={this.state.new_user_data.nu_last_name} />
						</div>
					</div>
					<div className="col-sm-3">
						<div className="input-group form-control well well-sm">
							<label className="input-group-addon">Age: </label>
							<input name="nu_age" className="form-control" onChange={this.handleInputChange} value={this.state.new_user_data.nu_age} />
						</div>
					</div>

					<div className="col-sm-3">
						<div className="input-group form-control well well-sm">
							<label className="input-group-addon">Thumbnail: </label>
							<input name="nu_thumbnail" className="form-control" onChange={this.handleInputChange} value={this.state.new_user_data.nu_thumbnail} />
						</div>
					</div>

					<div className="col-sm-12">
						<div className="input-group form-control well well-sm">
							<label className="input-group-addon">Description: </label>
							<input name="nu_description" className="form-control" onChange={this.handleInputChange} value={this.state.new_user_data.nu_description} />
						</div>
					</div>
					<div className="col-sm-12">
						<div className="well well-sm">
							<button className="form-control btn btn-primary">Add new user</button>
						</div>
					</div>

				</form>
			</section>
		)
	}
}

function mapStatesToProps(state){
	return {
		users: state.users
	}
}

function matchDispatchToProps(dispatch){
	return bindActionCreators({addUser: addUser}, dispatch);
}

export default connect(mapStatesToProps, matchDispatchToProps)(UserAdd);
