import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';


class UserDetails extends Component {

	render() {

		if (!this.props.user.id) {
			return <h4>[select a user]</h4>
		}

		return(
			<section>
				<div>Full Name: {this.props.user.first} {this.props.user.last}</div><div>Age: {this.props.user.age}</div>
				<div>Description: {this.props.user.description}</div>
				<div><img src={this.props.user.thumbnail} /></div>
			</section>
		)
	}
}


function mapStatesToProps(state){
	return {
		user: state.activeUser
	}
}

export default connect(mapStatesToProps)(UserDetails);



